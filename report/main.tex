%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diaz Essay
% LaTeX Template
% Version 2.0 (13/1/19)
%
% This template originates from:
% http://www.LaTeXTemplates.com
%
% Authors:
% Vel (vel@LaTeXTemplates.com)
% Nicolas Diaz (nsdiaz@uc.cl)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[12pt,a4paper]{diazessay} % Font size (can be 10pt, 11pt or 12pt)

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------
\usepackage[backend=biber,style=numeric-comp,sorting=none,sortcites=true,natbib=true]{biblatex}
\setcounter{biburlnumpenalty}{9000}
\setcounter{biburllcpenalty}{9000}
\addbibresource{references.bib}
\usepackage{siunitx}


\title{\textbf{Shiba-kun: In-House Healthcare Assistant} \\ {\Large\itshape
        Robot Assistant for People with Diabetes Type 2}} % Title and subtitle

\author{\textbf{Eiji Onchi} \\ \textit{University of Tsukuba \& TU Delft}} % Author and institution

\date{\today} % Date, use \date{} for no date

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title section

Advisers:

Prof.dr.ir. R.H.M. Goossens (TU Delft),

Dr. S.C.E. Klein Nagelvoort Schuit (Erasmus MC),

Dr. Seung Hee Lee (Univ. Tsukuba)
%----------------------------------------------------------------------------------------
%	ABSTRACT AND KEYWORDS
%----------------------------------------------------------------------------------------

%\renewcommand{\abstractname}{Summary} % Uncomment to change the name of the abstract to something else

\begin{abstract}
    By the year 2030, it is expected that 24.1\% of the population in the
    Netherlands will be more than 65 years old, a demographic that is
    susceptible to chronic disorders like diabetes mellitus.  Following the
    initiative of Delft University of Technology and Erasmus MC to design a
    \emph{''Consultation Room 2030''}, a 5-week joint research project with the
    University of Tsukuba was proposed to develop Shiba-kun, a
    \textbf{household robot assistant for patients with diabetes}, which will
    be part of the healthcare ecosystem in 2030.
\end{abstract}

\hspace*{3.6mm}\textit{Keywords:} future healthcare, robot, diabetes

\vspace{30pt} % Vertical whitespace between the abstract and first section

%----------------------------------------------------------------------------------------
%	ESSAY BODY
%----------------------------------------------------------------------------------------
\pagebreak
\section*{Introduction}

In a future with higher life expectancy, lower birth-rates, and super-aging
populations, society needs to account for health and long-term care of its
citizens. By the year 2030, it is expected that \emph{''one in every three
    [Japanese] people will be 65+ years old''}~\cite{muramatsu2011} and 34
countries will be considered super-aging societies~\cite{moody2014} including
the Netherlands, with an expected 24.1\% of its population within this age
range~\cite{giannakouris2010}.

That trend correlates to the increase of chronic disorders like diabetes
mellitus (DM). As reported in 2019, 25\% of people older than 65 years old has
DM~\cite{carrillo-larco2019}, 90\% of which are type 2
diabetes~\cite{goyal2019}. Part of the lifestyle of people with DM is to
control their diets and measure their glucose levels constantly.

The current option available to measure glucose are Blood Glucose Meters, which
use test strips to analyze a drop of blood. Inevitably, that means patients
need to prick part of their bodies (usually a fingertip) with a needle to
measure their glucose.  Stigma, fear of needles, and frustration related to
glucose readings make the already stressful disorder hard to
control~\cite{chua2014}. In the future, non-invasive alternatives like
GlucoWise~\cite{glucowise2019} will be preferred. Nevertheless it is necessary
to design a better self-care experience that can keep track of the patients
mood and well-being, while having more reliable measurements, which will aid
the care-giver provide better treatments and the patients to have a better
quality of life.

\subsection*{Joint Research Project}

Following the initiative of Delft University of Technology and Erasmus MC to
design a \emph{''Consultation Room 2030''}, a joint research project with the
University of Tsukuba was proposed to develop a \textbf{household robot
    assistant for patients with diabetes}, which will be part of the healthcare
ecosystem in 2030.

\pagebreak
\section{Shiba-kun: In-house Healthcare Assistant}

During five weeks at Delft University of Technology, the prototype for
Shiba-kun, a in-house healthcare assistant for patients with diabetes type 2
was developed.

\subsection{Physical Design}

To avoid the \emph{''Uncanny Valley''} (aversion toward robots that closely
resemble humans~\cite{mori2012}), Shiba-kun follows an animal-inspired shape
design.  It resembles a Japanese Shiba-Inu in the sitting position
(Figure~\ref{fig:robot}) to encourage users to consider the robot as a pet,
instead of just a healthcare device.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.9\textwidth]{robot}
    \caption[]{\bfseries{Shiba-kun: in-house healthcare assistant. Its shape resembles a
        sitting dog.}}
    \label{fig:robot}
\end{figure}

Accounting for single and multi-user households an NFC physical device was added as
the user id (Figure~\ref{fig:login}). This was decided to encourage physical
interaction with the device, establishing a spatial aspect to the interaction.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{login}
    \caption[]{\bfseries{NFC log in devices to encourage physical interaction with the
        device.}}
    \label{fig:login}
\end{figure}


\pagebreak
An Ultimaker 3D printer was used to fast prototype the chassis of the robot. A
base primer coating followed by spray paint was used to finish the prototype
(Figure~\ref{fig:prototype}).

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{prototype}
    \caption[]{\bfseries{Fast prototyping of Shiba-kun.}}
    \label{fig:prototype}
\end{figure}

\subsection{Input and Display Interface}

Shiba-kun uses two circular displays, one for the main content in the upper
area and another to add extra information around the knob
(Figure~\ref{fig:display}). To make the functional prototype, a \SI{7.85}{inch}
tablet was placed behind the 3D printed chassis.

The main input method is a \SI{35}{mm} knob, a familiar interface in physical
devices. Knobs are ideal to increase/decrease a value (e.g.\ volume control),
linear scrolling (e.g.\ radio frequency syntonization) or time control (e.g.
clock dial)~\cite{tan2015}, interactions that will be used to input glucose
data and review historical information in the robot.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{displays}
    \caption[]{\bfseries{Dual circular display. Upper area is the main screen
            while the bottom area adds information to the knob.}}
    \label{fig:display}
\end{figure}

Due to limitations in the prototype, minimal touch areas were implemented on
the upper screen to add more control options and dummy data.

\pagebreak
\subsection{Interface Design}

Based on the user interaction designed by van der
Velden~\cite{vandervelden2019} and Yan~\cite{yan2020}, and taking inspiration
in the user interface of circular displays~\cite{samsung2020}, a circular
visual interface was designed to display historical data of glucose
measurements. Circular plots are ideal to represent periodical data and time
dependent information. Figure~\ref{fig:ui} shows examples of these interfaces.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{ui}
    \caption[]{\bfseries{From left to right: van der Velden's Digital you,
            Yan's Scheme 3, Samsung Gear Activity Interface, Google Fit.}}
    \label{fig:ui}
\end{figure}

Instead of just a numerical value (often presented as $X$\si{mmol/l}), the
glucose value is computed with the historical information of the user and
categorized into five levels, represented with a thermal color scale:

\begin{itemize}[noitemsep]
    \item Extremely High (red): the current glucose value is too high from the
        recommended range and an immediate action must be taken by the user.
    \item High (orange): the current glucose value is higher that the recommended level
        and the user should take care of their condition, but no immediate
        action is required.
    \item Normal (green): the current glucose value is within the recommended level.
    \item Low (light blue): the current glucose value is lower than the recommended range
        and the user should take care of their condition, but no immediate
        action is required.
    \item Extremely Low (blue): the current glucose value is too low from the
        recommended range and an immediate action must be taken by the user.
\end{itemize}

A 24-hour dial with 00 hours starting at the bottom was chosen to display daily
historical data of glucose values. Inward values show lower glucose measures
and outward values represent higher ones.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.65\textwidth]{dial}
    \caption[]{\bfseries{24-hour circular plot representing daily glucose
            measurements.}}
    \label{fig:dial}
\end{figure}

Each measurement of the user is added to the plot and is shown as triangles
pointing at the measured time.  The central circle shows detailed information
on the currently selected measurement. Rotating the dial to the left selects
older measurements, while rotating it to the right selects newer records.

Different display layouts were tested to show the records. As shown in
Figure~\ref{fig:iterations}, the initial design showed data as a solid dial,
then the inside of the dial was obscured to highlight the outer border,
with the final version only showing the outer border.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{iter}
    \caption[]{\bfseries{Interface design iterations. From left to right,
            version 1 shows data as a solid dial, version 2 focuses the
            information on the outer border, and version 3 only shows to outer
            border to simplify the information.}}
    \label{fig:iterations}
\end{figure}

This layout makes comparing daily/weekly/monthly data visually easier, and
helps identify at a glance which days the user was within range and which days
there were fluctuations.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.8\textwidth]{hist}
    \caption[]{\bfseries{Historical data comparison.}}
    \label{fig:historical}
\end{figure}

\subsection{Recording Measurements}

For the functional prototype, manual input of the glucose reading is required.
By the year 2030, it is expected that automatic synchronized devices will be
available in the market and the robot Shiba-kun will update its information
from the cloud.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{input}
    \caption[]{\bfseries{Input of glucose measurement.}}
    \label{fig:input}
\end{figure}

After each measurement, the user can provide positive feedback to patients in
the same situation as the user. This feedback can motivate goal
pursuit~\cite{fishbach2010}. Positive feedback is helpful to foster long-term
motivation~\cite{burgers2015} and when patients feel that the feedback provided
is empathic and guiding, rather than critique, their acceptance
increases~\cite{dellasega2012}. Big data analysis will be used to classify the
user into clusters with people in the same condition to provide feedback among
those users. This interaction is non-verbal and no personal information is
shared among users.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{feedback}
    \caption[]{\bfseries{Positive feedback among users.}}
    \label{fig:feedback}
\end{figure}

This information will be stored into the user's account and can be reviewed
later by the user or analyzed by the physician.  Figure~\ref{fig:flow} shows an
example of the information flow.
The glucose analysis should be enhanced with information regarding physical
activity and caloric intake. In the future, such data can be synchronized from
other devices.

\begin{figure}[!h]
    \centering
    \includegraphics[width=1.0\textwidth]{flow}
    \caption[]{\bfseries{Information flow of Shiba-kun.}}
    \label{fig:flow}
\end{figure}

\pagebreak
\section{Development}

To create the working prototype, a \SI{7.85}{inch} android tablet (Android
4.2.1) was used to work as the displays and process the interface.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.7\textwidth]{tablet}
    \caption[]{\bfseries{Ainol Numy 3G Talos 7.85'' tablet.}}
    \label{fig:tablet}
\end{figure}


Android Studio 3.6.1 was used to program the device, with Kotlin as the main
programming language.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{android}
    \caption[]{\bfseries{Android Studio.}}
    \label{fig:android}
\end{figure}


\pagebreak
The knob is a rotary encoder connected to an Arduino Nano. To send the
information of the rotary encoder to the tablet, a Bluetooth module attached to
the microprocessor is wireless connected to the tablet.

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{arduino}
    \caption[]{\bfseries{Rotary encoder connected to an Arduino Nano and a
            Bluetooth module.}}
    \label{fig:arduino}
\end{figure}


\pagebreak
\section{Interaction Demo}

\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{demo}
    \caption[]{\bfseries{Example of the interaction with Shiba-kun.}}
    \label{fig:demo}
\end{figure}

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

%\bibliographystyle{author}

%%\bibliography{references.bib}

\pagebreak
\printbibliography[]


%----------------------------------------------------------------------------------------

\end{document}

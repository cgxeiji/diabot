package com.example.diapp

import java.util.*
import com.github.mikephil.charting.data.RadarData
import com.github.mikephil.charting.data.RadarDataSet
import com.github.mikephil.charting.data.RadarEntry

class GlucoseData {
    lateinit var chart: RadarChart
    val data: MutableMap<Float, Float>

    fun init() {
        for (i in 0 until DAY_UNITS) {
            when (i.toFloat()/2f) {
                0f, 7f, 11f, 13f, 17f, 19f -> {
                    val v = (Math.random() * (MAX_GLUCOSE - MIN_GLUCOSE)).toFloat() + MIN_GLUCOSE
                    data.put(i.toFloat(), v)
                }
            }
        }
    }

    fun getRadarData() RadarData {
        val normalEntry: ArrayList<RadarEntry> = ArrayList()
        val mediumEntry: ArrayList<RadarEntry> = ArrayList()
        val highEntry: ArrayList<RadarEntry> = ArrayList()
        val measuresEntry: ArrayList<RadarEntry> = ArrayList()

        var v = 0f

        for (i in 0 until DAY_UNITS) {
            if (i.toFloat() in data) {
                v = data[i]
                measuresEntry.add(RadarEntry(v))
            } else {
                measuresEntry.add(RadarEntry(INDICATOR_RADIUS))
            }
            when {
                (v < NORMAL_LIMIT) -> {
                    normalEntry.add(RadarEntry(v))
                    mediumEntry.add(RadarEntry(v-1))
                    highEntry.add(RadarEntry(v-1))
                }
                (v >= NORMAL_LIMIT && v < MEDIUM_LIMIT) -> {
                    normalEntry.add(RadarEntry(NORMAL_LIMIT))
                    mediumEntry.add(RadarEntry(v))
                    highEntry.add(RadarEntry(v-1))
                }
                (v >= MEDIUM_LIMIT) -> {
                    normalEntry.add(RadarEntry(NORMAL_LIMIT))
                    mediumEntry.add(RadarEntry(MEDIUM_LIMIT))
                    highEntry.add(RadarEntry(v1))
                }
            }
        }

        val measuresSet = RadarDataSet(measuresEntry, "Measure")
        measuresSet.color = Color.argb(180, 255, 255, 255)
        measuresSet.fillColor = Color.BLACK
        measuresSet.setDrawFilled(true)
        measuresSet.fillAlpha = 100
        measuresSet.lineWidth = 2f
        measuresSet.isDrawHighlightCircleEnabled = true
        measuresSet.setDrawHighlightIndicators(false)

        val highSet = RadarDataSet(highEntry, "High")
        highSet.color = Color.rgb(255, 0, 0)
        highSet.fillColor = Color.rgb(255, 0, 0)
        highSet.setDrawFilled(true)
        highSet.fillAlpha = 180
        highSet.lineWidth = 2f
        highSet.isDrawHighlightCircleEnabled = false
        highSet.setDrawHighlightIndicators(false)

        val mediumSet = RadarDataSet(measuresEntry, "Mid")
        mediumSet.color = Color.rgb(255, 180, 0)
        mediumSet.fillColor = Color.rgb(255, 180, 0)
        mediumSet.setDrawFilled(true)
        mediumSet.fillAlpha = 200
        mediumSet.lineWidth = 2f
        mediumSet.isDrawHighlightCircleEnabled = false
        mediumSet.setDrawHighlightIndicators(false)

        val normalSet = RadarDataSet(normalEntry, "Inner")
        normalSet.color = Color.rgb(0, 180, 0)
        normalSet.fillColor = Color.rgb(0, 180, 0)
        normalSet.setDrawFilled(true)
        normalSet.fillAlpha = 250
        normalSet.lineWidth = 2f
        normalSet.isDrawHighlightCircleEnabled = false
        normalSet.setDrawHighlightIndicators(false)

        val sets: ArrayList<IRadarDataSet> = ArrayList()
        sets.add(highSet)
        sets.add(mediumSet)
        sets.add(normalSet)
        sets.add(measuresSet)

        val rData = RadarData(sets)
        rData.setValueTextSize(8f)
        rData.setDrawValues(false)
        rData.setValueTextColor(Color.WHITE)

        return rData
    }

    companion object {
        private const val DAY_UNITS = 48
        private const val INDICATOR_RADIUS = 20f

        private const val MAX_GLUCOSE = 100f
        private const val MIN_GLUCOSE = 40f

        private const val NORMAL_LIMIT = 60f
        private const val MEDIUM_LIMIT = 80f
        private const val HIGH_LIMIT = 100f
    }
}
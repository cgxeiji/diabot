package com.example.diapp

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.text.TextPaint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.animation.LinearInterpolator
import kotlin.math.absoluteValue
import kotlin.math.ln
import kotlin.math.pow

/**
 * TODO: document your custom view class.
 */
class CircleView : View {

    private var currentColor = GlucoseData.NORMAL_COLOR
    var colorChangeEnabled = false

    private var _value: Float = 0f
    var value: Float
        get() = _value
        set(value) {
            _value = value

            animator?.setFloatValues(animator?.animatedValue as Float, adjust(_value))
            animator?.start()

            if (colorChangeEnabled) {
                when {
                    _value < GlucoseData.LOW_H_LEVEL -> {
                        changeColor(currentColor, GlucoseData.LOW_H_COLOR)
                    }
                    _value < GlucoseData.LOW_M_LEVEL -> {
                        changeColor(currentColor, GlucoseData.LOW_M_COLOR)
                    }
                    _value < GlucoseData.NORMAL_LEVEL -> {
                        changeColor(currentColor, GlucoseData.NORMAL_COLOR)
                    }
                    _value < GlucoseData.HIGH_M_LEVEL -> {
                        changeColor(currentColor, GlucoseData.HIGH_M_COLOR)
                    }
                    else -> {
                        changeColor(currentColor, GlucoseData.HIGH_H_COLOR)
                    }
                }
                animC?.start()
            }
        }

    private var animator: ValueAnimator? = null
    private var animC: ValueAnimator? = null

    private val fromHSV = FloatArray(3)
    private val toHSV = FloatArray(3)

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.CircleView, defStyle, 0
        )

        animator = ValueAnimator.ofFloat(0f, 0.5f)
        animator?.duration = 500
        animator?.interpolator = LinearInterpolator()

        animator?.addUpdateListener {
            factor = (it.animatedValue as Float)
            invalidate()
        }

        animator?.start()

        animC = ValueAnimator.ofFloat(0f, 1f)
        animC?.duration = 1000
        animC?.interpolator = LinearInterpolator()

        val currentHSV = FloatArray(3)
        animC?.addUpdateListener {
            currentHSV[0] = fromHSV[0] + (toHSV[0] - fromHSV[0])*it.animatedFraction
            currentHSV[1] = fromHSV[1] + (toHSV[1] - fromHSV[1])*it.animatedFraction
            currentHSV[2] = fromHSV[2] + (toHSV[2] - fromHSV[2])*it.animatedFraction

            currentColor = Color.HSVToColor(currentHSV)
            invalidate()
        }

        changeColor(GlucoseData.LOW_H_COLOR, GlucoseData.NORMAL_COLOR)

        a.recycle()
    }

    private fun changeColor(from: Int, to: Int) {
        Color.colorToHSV(from, fromHSV)
        Color.colorToHSV(to, toHSV)

        animC?.start()
    }

    fun setColor(c: Int) {
        Color.colorToHSV(GlucoseData.NORMAL_COLOR, fromHSV)
        Color.colorToHSV(c, toHSV)
    }

    private var factor = 1f

    private fun adjust(v: Float): Float{
        return (v / 40f) + .4f
        /*
        val x = (v - 5f) / 200
        val cubic = x.absoluteValue.pow(0.333f) * 3
        return when {
            x > 0 -> cubic
            else -> -cubic
        } / 10f + 0.5f
        */
    }

    private val paint = Paint()
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        val w = width.toFloat()
        val h = height.toFloat()

        paint.color = currentColor

        canvas.drawCircle(w/2, h/2, w/2*factor, paint)
    }
}

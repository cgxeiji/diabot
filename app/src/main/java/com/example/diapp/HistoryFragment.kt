package com.example.diapp

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Vibrator
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.BounceInterpolator
import android.view.animation.LinearInterpolator
import android.view.animation.OvershootInterpolator
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.RadarChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.rey.material.widget.FrameLayout
import com.rey.material.widget.ProgressView
import kotlinx.android.synthetic.main.activity_fullscreen.*
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.fragment_history.view.*
import java.text.DateFormat
import java.util.*
import kotlin.math.absoluteValue

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HistoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HistoryFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var chart: RadarChart
    private lateinit var glucose: GlucoseData

    private lateinit var vLoading: ProgressView
    private lateinit var vHide: android.widget.FrameLayout
    private lateinit var vClose: android.widget.FrameLayout

    private var debounce = MAX_DEBOUNCE

    private var loaded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_history, container, false)

        vLoading = view.findViewById(R.id.progressBar)
        vHide = view.findViewById(R.id.pbFrame)
        vClose = view.findViewById(R.id.closeFrame)

        vClose.alpha = 0f

        glucose = GlucoseData(view.context)
        glucose.load()
        glucose.selectDate(glucose.days.last()[Calendar.DATE])

        glucose.setOnLimitReachedListener(object: GlucoseData.OnLimitReachedListener {
            override fun onLimitReached(direction: Int) {
                when (direction) {
                    GlucoseData.MIN -> {
                        toClose()
                    }
                    GlucoseData.MAX -> {
                        toClose()
                    }
                }
            }
        })


        chart = view.findViewById(R.id.chart1)
        chart.setBackgroundColor(Color.argb(180, 0, 0, 0))

        chart.setOnChartValueSelectedListener(object: OnChartValueSelectedListener {
            var flag = false
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                when (flag) {
                    true -> {
                        flag = false
                        Log.d(TAG, "refreshing glucose data")
                        glucose.simulate()
                        chart.data = glucose.getRadarData()
                        chart.invalidate()

                        chart.animateXY(1200, 0, Easing.EaseInOutSine)

                        ci = 0
                        selectRecord(1)
                    }
                    else -> {
                        flag = true
                    }
                }
            }

            var ci = 0
            override fun onNothingSelected() {
                Log.d(TAG, "selecting glucose data")
                ci = (ci + 1) % glucose.currentSize()
                selectRecord(ci)
            }
        })
        chart.description.isEnabled = false

        chart.webLineWidth = 1f
        chart.webColor = Color.LTGRAY
        chart.webLineWidthInner = 1f
        chart.webColorInner = Color.LTGRAY
        chart.webAlpha = 100
        chart.rotationAngle = 90f
        chart.isRotationEnabled = false

        chart.data = glucose.getRadarData()
        chart.data = glucose.select(glucose.currentSize() - 1)
        //chart.data = glucose.select(2)


        val xAxis = chart.xAxis
        //xAxis.typeface = tfLight
        xAxis.textSize = 20f
        xAxis.yOffset = 0f
        xAxis.xOffset = 0f


        xAxis.valueFormatter = object : ValueFormatter() {
            private val mActivities =
                IntArray(24) { it * 1 }

            override fun getFormattedValue(value: Float): String {
                return when (value.toInt() % 2){
                    0 -> mActivities[value.toInt()/2 % mActivities.size].toString()
                    else -> "  "
                }

            }
        }

        xAxis.textColor = Color.WHITE

        val yAxis = chart.yAxis
        //yAxis.typeface = tfLight
        yAxis.setLabelCount(1, true)
        yAxis.textSize = 9f
        yAxis.axisMinimum = 0f
        yAxis.axisMaximum = 10f
        yAxis.setDrawLabels(false)

        val l = chart.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        //l.typeface = tfLight
        l.xEntrySpace = 7f
        l.yEntrySpace = 5f
        l.textColor = Color.WHITE

        val dateF = DateFormat.getDateInstance(DateFormat.MEDIUM)
        val timeF = DateFormat.getTimeInstance(DateFormat.SHORT)
        view.textView.text = String.format(
            "%s\n%s\n%.1f mmol/L",
            dateF.format(glucose.selected.time.time),
            timeF.format(glucose.selected.time.time),
            glucose.selected.value
        )

        return view
    }

    override fun onResume() {
        super.onResume()
        /*
        vLoading.resetLoading()
        vLoading.startIndeterminate()

         */
        Log.d(TAG, "starting")

        Handler().postDelayed({
            chart.invalidate()
            chart.animateXY(1200, 0, Easing.EaseInOutSine)
            vHide.animate().alpha(0f).setDuration(1000)
                .setInterpolator(AccelerateInterpolator()).start()
            vLoading.animate().alpha(0f).setDuration(1000)
                .setInterpolator(AccelerateInterpolator()).start()
            Handler().postDelayed({
                vLoading.visibility = View.GONE
                vHide.visibility = View.GONE
                loaded = true
            }, 1200)
        }, 2500)
    }

    var closeVisible = false
    private var closeBusy = false
    private fun toClose() {
        if (closeVisible) return
        if (closeBusy) return

        closeBusy = true
        vClose.apply {
            scaleX = 0f
            scaleY = 0f
        }
        vClose.animate().setDuration(800).setInterpolator(OvershootInterpolator())
            .scaleX(1f).scaleY(1f).start()
        vClose.animate().setDuration(500).setInterpolator(LinearInterpolator())
            .alpha(1f)

        Handler().postDelayed({
            closeBusy = false
            closeVisible = true
        }, 1200)

        (activity as FullscreenActivity).knob.setMode(Knob.YES_NO)
    }

    private fun closeInflate(v: Float) {
        if (!closeVisible) return
        if (closeBusy) return

        vClose.animate().setDuration(300).setInterpolator(OvershootInterpolator())
            .scaleX(v).scaleY(v).start()
    }

    private fun noClose() {
        if (closeBusy) return
        if (!closeVisible) return

        closeBusy = true
        vClose.animate().setDuration(500).setInterpolator(LinearInterpolator())
            .scaleX(0f).scaleY(0f).start()
        vClose.animate().setDuration(500).setInterpolator(LinearInterpolator())
            .alpha(0f)

        Handler().postDelayed({
            closeVisible = false
            closeBusy = false
        }, 700)

        (activity as FullscreenActivity).knob.setMode(Knob.PREV_NEXT)
    }

    private fun selectRecord(index: Int) {
        chart.data = glucose.select(index)
        chart.invalidate()

        (context?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator).vibrate(100)

        val dateF = DateFormat.getDateInstance(DateFormat.MEDIUM)
        val timeF = DateFormat.getTimeInstance(DateFormat.SHORT)
        textView.text = String.format(
            "%s\n%s\n%.1f mmol/L",
            dateF.format(glucose.selected.time.time),
            timeF.format(glucose.selected.time.time),
            glucose.selected.value
        )
    }

    fun selected(angle: Int, direction: Int) {
        if (!loaded) return

        if (closeVisible) {
            when (direction) {
                Knob.LEFT -> {
                    if (debounce < 0) {
                        (activity as FullscreenActivity).changeFragment("Home")
                    }
                    closeInflate((MAX_DEBOUNCE - debounce) / 2.5f + 1f)
                    debounce -= 1
                }
                Knob.RIGHT -> {
                    debounce = MAX_DEBOUNCE
                    noClose()
                }
            }
            return
        }

        when (direction) {
            Knob.RIGHT -> {
                selectRecord(glucose.index + 1)
            }
            Knob.LEFT -> {
                selectRecord(glucose.index - 1)
            }
        }
    }

    override fun onPause() {
        super.onPause()

        //glucose.save()
    }

    companion object {
        private const val TAG = "CGx History Fragment"

        private const val MAX_DEBOUNCE = 2

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HistoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HistoryFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}

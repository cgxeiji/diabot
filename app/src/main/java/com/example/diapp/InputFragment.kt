package com.example.diapp

import android.animation.ObjectAnimator
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_input.*
import kotlinx.android.synthetic.main.fragment_input.view.*
import kotlin.math.absoluteValue
import kotlin.math.ln


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [InputFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class InputFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var animation: ObjectAnimator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_input, container, false)

        //animation = ObjectAnimator.ofFloat(view.imgNeedle, "rotation", 0f)
        //animation?.duration = 500
        //animation?.interpolator = LinearInterpolator()

        view.fab.setOnClickListener {
            //(activity as FullscreenActivity).changeFragment("History")
            view.fab.isClickable = false

            val glucose = GlucoseData(context!!)
            //glucose.simulate()
            //glucose.save()
            glucose.load()
            glucose.add(current)
            glucose.save()

            view.fab.animate().translationY(-1000f).setDuration(500)
                .setInterpolator(AccelerateInterpolator()).start()
            Handler().postDelayed({
                (activity as FullscreenActivity).showResult(current)
            }, 400)
        }

        view.button.visibility = View.INVISIBLE

        view.tvQuestion.text = "What is your\ncurrent glucose?"

        view.circleView.colorChangeEnabled = false
        view.circleView.setColor(Color.parseColor("#434b54"))
        view.circleView.value = current
        view.tvValue.text = String.format("%.1f\nmmol/L", current)

        return view
    }

    private var prevAngle = 0
    private var current = 5f
    fun knob(angle: Int) {
        var delta = (angle - prevAngle)
        when {
            delta < 0 && delta.absoluteValue > (delta + 360).absoluteValue -> {
                delta += 360
            }
            delta >= 0 && delta.absoluteValue > (delta - 360).absoluteValue -> {
                delta -= 360
            }
        }
        current += (delta) / 200f

        when {
            current <= 0f -> current = 0f
        }

        //animation?.setFloatValues(adjust(current))
        //animation?.start()

        circleView.value = current

        tvValue.text = String.format("%.1f\nmmol/L", current)
        prevAngle = angle
    }

    private fun adjust(v: Float): Float {
        val x = (v - 5f) * 10
        val cubic = ln(x.absoluteValue + 1) * 10
        return when {
            x > 0 -> cubic
            else -> -cubic
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment InputFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            InputFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}

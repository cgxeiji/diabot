package com.example.diapp

import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.AnimationUtils
import android.widget.CalendarView
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.RadarChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.RadarData
import com.github.mikephil.charting.data.RadarDataSet
import com.github.mikephil.charting.data.RadarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import kotlinx.android.synthetic.main.fragment_test.*
import kotlinx.android.synthetic.main.fragment_test.view.*
import java.text.DateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TestFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TestFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TestFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    private var current: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    fun ImageViewAnimatedChange(
        c: Context?,
        v: ImageView,
        new_image: Int
    ) {
        val anim_out = AnimationUtils.loadAnimation(c, android.R.anim.fade_out)
        val anim_in = AnimationUtils.loadAnimation(c, android.R.anim.fade_in)
        anim_out.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationRepeat(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {
                v.setImageResource(new_image)
                anim_in.setAnimationListener(object : AnimationListener {
                    override fun onAnimationStart(animation: Animation) {}
                    override fun onAnimationRepeat(animation: Animation) {}
                    override fun onAnimationEnd(animation: Animation) {}
                })
                v.startAnimation(anim_in)
            }
        })
        v.startAnimation(anim_out)
    }

    private lateinit var tvTime: TextView
    private lateinit var tvLast: TextView
    private lateinit var tvGlucose: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_test, container, false)
        val glucose = GlucoseData(context!!)
        glucose.load()
        val last = glucose.getLast()

        tvTime = view.findViewById(R.id.tvTime)
        tvLast = view.findViewById(R.id.tvLast)
        tvGlucose = view.findViewById(R.id.tvGlucose)
        val timeF = DateFormat.getTimeInstance(DateFormat.SHORT)
        val dateF = DateFormat.getDateInstance(DateFormat.MEDIUM)
        Timer().scheduleAtFixedRate(object: TimerTask() {
            override fun run() {
                activity?.runOnUiThread {
                    val t = Calendar.getInstance()

                    tvTime.text = String.format(
                        "%s, %s",
                        dateF.format(t.time),
                        timeF.format(t.time)
                    )

                    tvLast.text = DateUtils.getRelativeTimeSpanString(last.time.timeInMillis)

                }
            }
        }, 0, 500)
        tvGlucose.text = String.format("%.1f mmol/L", last.value)

        when (GlucoseData.getLimit(last.value)) {
            GlucoseData.LOW_H_LEVEL, GlucoseData.HIGH_H_LEVEL -> {
                view.imgQuality.setImageResource(R.drawable.one_star)
            }
            GlucoseData.LOW_M_LEVEL, GlucoseData.HIGH_M_LEVEL -> {
                view.imgQuality.setImageResource(R.drawable.two_stars)
            }
            GlucoseData.NORMAL_LEVEL -> {
                view.imgQuality.setImageResource(R.drawable.three_stars)
            }
        }


        return view
    }


    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    fun selected(s: String) {
        //textView.text = s
        if (s != current) {
            when (s) {
                "Yes" -> {
                    ImageViewAnimatedChange(context, img, R.drawable.good)
                    //img.setColorFilter(Color.argb(50, 0, 255, 0))
                }
                "No" -> {
                    ImageViewAnimatedChange(context, img, R.drawable.home)
                    //img.setColorFilter(Color.argb(50, 255, 0, 0))
                }
            }
        }
        current = s
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        private const val TAG = "CGx Test Fragment"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TestFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TestFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}

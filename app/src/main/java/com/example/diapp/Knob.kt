package com.example.diapp

import android.animation.ObjectAnimator
import android.content.Context
import android.opengl.Visibility
import android.os.Handler
import android.os.Vibrator
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.FrameLayout
import androidx.core.util.rangeTo
import androidx.interpolator.view.animation.FastOutSlowInInterpolator
import kotlinx.android.synthetic.main.knob_view.view.*
import kotlin.math.*


/**
 * TODO: document your custom view class.
 */
class Knob : FrameLayout {

    private val delayHandler = Handler()
    private var kDelay = false
    private val delayer = Runnable {
        kDelay = false
    }

    private var animation: ObjectAnimator

    constructor(context: Context) : super(context) {
        init(null, 0)
        animation = ObjectAnimator.ofFloat(dial, "rotation", 0f)
        animation.duration = 500
        animation.interpolator = LinearInterpolator()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
        animation = ObjectAnimator.ofFloat(dial, "rotation", 0f)
        animation.duration = 500
        animation.interpolator = DecelerateInterpolator()
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
        animation = ObjectAnimator.ofFloat(dial, "rotation", 0f)
        animation.duration = 500
        animation.interpolator = LinearInterpolator()
    }

    interface OnKnobSelectedListener {
        fun onKnobSelected(selection: String, angle: Int, direction: Int)
    }

    private var mOnSelectedListener: OnKnobSelectedListener? = null

    fun setOnKnobSelectedListener(l: OnKnobSelectedListener?) {
        this.mOnSelectedListener = l
    }

    interface OnKnobDirectionListener {
        fun onKnobDirection(direction: Int)
    }

    private var mOnDirectionListener: OnKnobDirectionListener? = null

    fun setOnKnobDirectionListener(l: OnKnobDirectionListener?) {
        this.mOnDirectionListener = l
    }

    val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

    var oneShot = true
    val tickAngle = 360/2

    var dTheta = 0f
    val tSpeed = 0.5f
    private var isRotating = true

    fun rotate(angle: Float) {
        if (isRotating) {
            animation.setFloatValues(angle)
            animation.start()
        }

        dTheta = angle % 360
        if (dTheta < 0) {
            dTheta += 360
        }

        val mTick = abs((dTheta + 10) % tickAngle)
        var s = String.format("dial: %f", dTheta)
        s += String.format("\ntick: %f", mTick)

        mOnSelectedListener?.onKnobSelected("", dTheta.toInt(), direction(dTheta))

        mOnDirectionListener?.onKnobDirection(direction(dTheta))

        /*
        if ( mTick in 0f..20f || mTick in 160f..180f ) {
            if (oneShot) {
                oneShot = false
                //vibrator.vibrate(500)
                s += String.format("\nclick: %f", round(abs(dTheta) / tickAngle))
                when(round(abs(dTheta) / tickAngle)){
                    1f -> {
                        mOnSelectedListener?.onKnobSelected("Yes", dTheta.toInt())
                    }
                    0f, 2f -> {
                        mOnSelectedListener?.onKnobSelected("No", dTheta.toInt())
                    }
                }
            }
        } else {
            oneShot = true
            mOnSelectedListener?.onKnobSelected("", dTheta.toInt())
        }
        */

        txtDebug.text = s
    }

    private var prevAngle = 0f
    private fun direction(angle: Float): Int {
        if (prevAngle == 0f) {
            prevAngle = angle
        }

        var delta = (angle - prevAngle)
        when {
            delta < 0 && delta.absoluteValue > (delta + 360).absoluteValue -> {
                delta += 360
            }
            delta >= 0 && delta.absoluteValue > (delta - 360).absoluteValue -> {
                delta -= 360
            }
        }

        prevAngle = angle

        return when {
            delta > 0f -> {
                RIGHT
            }
            delta < 0f -> {
                LEFT
            }
            else -> {
                NO_DIRECTION
            }
        }
    }

    fun setMode(m: String) {
        animation.cancel()
        dial.rotation = 0f
        when (m) {
            PLUS_MINUS -> {
                changeImage(R.drawable.plus_minus)
                isRotating = false
            }
            START -> {
                changeImage(R.drawable.dial)
                isRotating = true
            }
            CONTINUE -> {
                changeImage(R.drawable.cont_dial)
                isRotating = true
            }
            PREV_NEXT -> {
                changeImage(R.drawable.prev_next_dial)
                isRotating = false
            }
            YES_NO -> {
                changeImage(R.drawable.yes_no_dial)
                isRotating = false
            }
            NO_YES -> {
                changeImage(R.drawable.no_yes_dial)
                isRotating = false
            }
            LOG_HIST -> {
                changeImage(R.drawable.log_hist)
                isRotating = false
            }
        }
    }

    fun changeImage(res: Int) {
        dial.animate().setDuration(500).setInterpolator(LinearInterpolator())
            .alpha(0f).start()
        Handler().postDelayed({
             dial.setImageResource(res)
            dial.animate().setDuration(500).setInterpolator(LinearInterpolator())
                .alpha(1f).start()
        },500)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.Knob, defStyle, 0
        )

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.knob_view, this)

        knobFrame.setOnTouchListener { _, event ->
            val cx = center.x + center.width/2
            val cy = center.y + center.height/2

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    val dx = event.x - cx
                    val dy = event.y - cy

                    //if (!kDelay) {
                        kDelay = true
                        dTheta = (atan2(dy, dx) / PI * 180).toFloat()
                    //}
                }
                MotionEvent.ACTION_MOVE -> {
                    val dx = event.x - cx
                    val dy = event.y - cy

                    val theta = atan2(dy, dx) / PI * 180 + 180

                    /*
                    val dt = theta.toFloat() - dTheta
                    if (abs(dt) < 90f) {
                        dial.rotation += (dt) * tSpeed
                        dial.rotation %= 360f
                    }
                    */
                    dTheta = theta.toFloat()

                    rotate(dTheta)
                }
                MotionEvent.ACTION_UP -> {
                    //delayHandler.removeCallbacks(delayer)
                    //delayHandler.postDelayed(delayer, 2000)
                }
            }

            true
        }

        txtDebug.visibility = View.INVISIBLE

        a.recycle()
    }

    companion object {
        const val PLUS_MINUS = "plus_minus"
        const val START = "start"
        const val CONTINUE = "continue"
        const val PREV_NEXT = "previous_next"
        const val YES_NO = "yes_no"
        const val NO_YES = "no_yes"
        const val LOG_HIST = "log_history"

        const val RIGHT = 1
        const val LEFT = 2
        const val NO_DIRECTION = 0
    }
}

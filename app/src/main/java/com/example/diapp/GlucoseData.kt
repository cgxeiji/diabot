package com.example.diapp

import android.content.Context
import android.graphics.Color
import android.util.Log
import com.github.mikephil.charting.data.RadarData
import com.github.mikephil.charting.data.RadarDataSet
import com.github.mikephil.charting.data.RadarEntry
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet
import java.io.File
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.absoluteValue
import kotlin.math.pow

class GlucoseData(ctx: Context) {
    private lateinit var rd: RadarData
    private val data: MutableMap<Calendar, Float> = mutableMapOf()
    private val database: ArrayList<Data> = arrayListOf()
    private val context = ctx
    var selected = Data(Calendar.getInstance(), 0f)

    var maxDate = Calendar.getInstance().apply {
        set(0, 0, 0)
    }
    var minDate = Calendar.getInstance().apply {
        set(9999, 0, 0)
    }

    class Data(t: Calendar, v: Float) {
        val time = t
        val value = v
    }

    init {
        //reload()
    }

    interface OnLimitReachedListener {
        fun onLimitReached(direction: Int)
    }

    private var mOnLimitReachedListener: OnLimitReachedListener? = null

    fun setOnLimitReachedListener(l: OnLimitReachedListener?) {
        this.mOnLimitReachedListener = l
    }

    private fun simulateDay(month: Int, date: Int): ArrayList<Data> {
        val list: ArrayList<Data> = arrayListOf()

        var t = Calendar.getInstance()
        var v = 0f
        var hour = 0

        /*
        t.set(2020, month,date, 0,0)
        v = (Math.random() * (MAX_GLUCOSE - MIN_GLUCOSE)).toFloat() + MIN_GLUCOSE
        list.add(Data(t, v))
        */

        t = Calendar.getInstance()
        hour = (Math.random() * (9 - 5)*60).toInt() + 5*60
        t.set(2020, month,date, hour / 60,hour % 60)
        v = (Math.random() * (MAX_GLUCOSE - MIN_GLUCOSE)).toFloat() + MIN_GLUCOSE
        list.add(Data(t, v))

        t = Calendar.getInstance()
        hour = (Math.random() * (12 - 10)*60).toInt() + 10*60
        t.set(2020, month,date, hour / 60,hour % 60)
        v = (Math.random() * (MAX_GLUCOSE - MIN_GLUCOSE)).toFloat() + MIN_GLUCOSE
        list.add(Data(t, v))

        t = Calendar.getInstance()
        hour = (Math.random() * (14 - 12)*60).toInt() + 12*60
        t.set(2020, month,date, hour / 60,hour % 60)
        v = (Math.random() * (MAX_GLUCOSE - MIN_GLUCOSE)).toFloat() + MIN_GLUCOSE
        list.add(Data(t, v))

        t = Calendar.getInstance()
        hour = (Math.random() * (18 - 17)*60).toInt() + 17*60
        t.set(2020, month,date, hour / 60,hour % 60)
        v = (Math.random() * (MAX_GLUCOSE - MIN_GLUCOSE)).toFloat() + MIN_GLUCOSE
        list.add(Data(t, v))

        t = Calendar.getInstance()
        hour = (Math.random() * (21 - 18)*60).toInt() + 18*60
        t.set(2020, month,date, hour / 60,hour % 60)
        v = (Math.random() * (MAX_GLUCOSE - MIN_GLUCOSE)).toFloat() + MIN_GLUCOSE
        list.add(Data(t, v))

        return list
    }

    fun simulate() {
        database.clear()
        database.addAll(simulateDay(2, 1))
        database.addAll(simulateDay(2, 2))
        database.addAll(simulateDay(2, 3))
        database.addAll(simulateDay(2, 4))
        database.addAll(simulateDay(2, 5))
        database.addAll(simulateDay(2, 6))
        database.addAll(simulateDay(2, 7))

        selectDate(7)
    }

    val sizeDates: Int
        get() {
            //FIXME: change sizeDates to actual value
            return 3
        }

    var currentDate = 2
    val days: ArrayList<Calendar> = arrayListOf()
    var dateIndex = 0

    fun nextDate() {
        dateIndex = when {
            dateIndex + 1 < days.size -> dateIndex + 1
            else -> days.size - 1
        }
        val date = days[dateIndex][Calendar.DATE]

        selectDate(date)
    }

    fun prevDate() {
        dateIndex = when {
            dateIndex - 1 >= 0 -> dateIndex - 1
            else -> 0
        }
        val date = days[dateIndex][Calendar.DATE]

        selectDate(date)
    }

    fun selectDate(date: Int) {
        val db = database.sortedBy { it.time }

        /*
        db.forEach {
            Log.d(TAG, it.time[Calendar.DATE].toString())
        }
        */

        val start = db.indexOfFirst { it.time.get(Calendar.DATE) == date }
        val end = db.indexOfLast { it.time.get(Calendar.DATE) == date }

        if (start == -1) {
            return
        }
        if (end == -1) {
            return
        }
        currentDate = date

        data.clear()
        var t = Calendar.getInstance()

        if (start - 1 > 0) {
            t.timeInMillis = db[start].time.timeInMillis
            t.set(Calendar.HOUR_OF_DAY, 0)
            t.set(Calendar.MINUTE, 0)
            t.set(Calendar.SECOND, 0)
            data[t] = db[start - 1].value
        }

        for (i in start..end) {
            data[db[i].time] = db[i].value
        }

        if (end + 1 < db.size) {
            t = Calendar.getInstance()
            t.timeInMillis = db[end].time.timeInMillis
            t.set(Calendar.HOUR_OF_DAY, 23)
            t.set(Calendar.MINUTE, 59)
            t.set(Calendar.SECOND, 59)
            data[t] = db[end + 1].value
        }
    }

    fun reload() {
        database.clear()
        val l = simulateDay(2, 2)

        database.addAll(l)

        selectDate(2)
    }

    fun currentSize(): Int {
        return data.size
    }

    private val selectionEntry: ArrayList<RadarEntry> = ArrayList(MutableList(DAY_UNITS) { RadarEntry(MIN_LIMIT) })
    var index: Int = 1
    fun select(i: Int): RadarData {
        Log.d(TAG, "Got " + i.toString())
        val prevI = index
        index = i
        when {
            index <= 0 -> {
                index = 0
                //Log.d(TAG, data.keys.elementAt(index).timeInMillis.toString() + " vs " + minDate.timeInMillis.toString())
                when {
                    data.keys.elementAt(index) == minDate -> {
                        Log.d(TAG, minDate.toString())
                        mOnLimitReachedListener?.onLimitReached(MIN)
                    }
                    selected.time > minDate -> {
                        //selectDate(currentDate - 1)
                        prevDate()
                        getRadarData()
                        index = currentSize() - 2
                    }
                    else -> {
                        index = prevI
                        return rd
                    }
                }
            }
            index >= currentSize() - 1 -> {
                val i = currentSize() - 1
                when {
                    data.keys.elementAt(i) == maxDate -> {
                        when {
                            index >= currentSize() -> {
                                mOnLimitReachedListener?.onLimitReached(MAX)
                            }
                        }
                        index = currentSize() - 1
                    }
                    selected.time < maxDate -> {
                        //selectDate(currentDate + 1)
                        nextDate()
                        getRadarData()
                        index = 1
                    }
                    else -> {
                        index = prevI
                        return rd
                    }
                }
            }
        }
        Log.d(TAG, "Selecting " + index.toString())

        selectionEntry[selected.time[Calendar.HOUR_OF_DAY]*2 + selected.time[Calendar.MINUTE]/30] = RadarEntry(MIN_LIMIT)
        val k = data.keys.elementAt(index)
        selected = Data(k, data[k]!!)
        val v = adjust(selected.value) + CHART_OFFSET

        selectionEntry[k[Calendar.HOUR_OF_DAY]*2 + selected.time[Calendar.MINUTE]/30] = RadarEntry(v)

        var c = Color.BLACK

        when {
            v < LOW_H_LIMIT -> {
                c = LOW_H_COLOR
            }
            v < LOW_M_LIMIT -> {
                c = LOW_M_COLOR
            }
            v < NORMAL_LIMIT -> {
                c = NORMAL_COLOR
            }
            v < HIGH_M_LIMIT -> {
                c = HIGH_M_COLOR
            }
            else -> {
                c = HIGH_H_COLOR
            }
        }

        val set = RadarDataSet(selectionEntry, "Selected")
        formatSet(set,
            c,
            c,
            200
        )
        set.lineWidth = INDICATOR_LINE_WIDTH

        rd.dataSets[rd.dataSetCount - 1] = set

        return rd
    }

    private fun getConstantRadarDataSet(value: Float): RadarDataSet {
        val entries: ArrayList<RadarEntry> = ArrayList(MutableList(DAY_UNITS) {
            RadarEntry(value)
        })

        val rds = RadarDataSet(entries, "")
        formatSet(rds,
            Color.argb(80, 255, 255, 255),
            Color.BLACK,
            0
        )

        return rds
    }

    fun getRadarData(): RadarData {
        val normalEntry: ArrayList<RadarEntry> = ArrayList()
        val highMEntry: ArrayList<RadarEntry> = ArrayList()
        val highHEntry: ArrayList<RadarEntry> = ArrayList()

        val coverEntry: ArrayList<RadarEntry> = ArrayList()
        val coverEntry2: ArrayList<RadarEntry> = ArrayList()

        val measuresEntry: ArrayList<RadarEntry> = ArrayList()

        val lowMEntry: ArrayList<RadarEntry> = ArrayList()
        val lowHEntry: ArrayList<RadarEntry> = ArrayList()

        var v = 0f
        var vStart = 0
        var vEnd = 0

        val indexes: MutableMap<Int, Calendar> = mutableMapOf()

        for (t in data.keys) {
            indexes[t[Calendar.HOUR_OF_DAY]*2 + t[Calendar.MINUTE] / 30] = t
        }
        Log.d(TAG, indexes.toString())

        for (i in 0 until DAY_UNITS) {
            when {
                i in indexes.keys -> {
                    vStart = i
                    v = data[indexes[vStart]]!!
                    v = adjust(v) + CHART_OFFSET
                    vEnd = indexes.keys.elementAtOrElse(indexes.keys.indexOf(vStart) + 1) {indexes.keys.last()}

                    when {
                        i == 0 -> {
                            measuresEntry.add(RadarEntry(INDICATOR_RADIUS))
                        }
                        i == DAY_UNITS - 1 -> {
                            measuresEntry.add(RadarEntry(INDICATOR_RADIUS))
                        }
                        else -> {
                            measuresEntry.add(RadarEntry(v))
                        }
                    }
                }
                vEnd == vStart -> {
                    v = INDICATOR_RADIUS
                    measuresEntry.add(RadarEntry(INDICATOR_RADIUS))
                }
                else -> {
                    val s = adjust(data[indexes[vStart]]!!) + CHART_OFFSET
                    val e = adjust(data[indexes[vEnd]]!!) + CHART_OFFSET

                    v = (e - s) / (vEnd - vStart).toFloat() *
                            (i - vStart).toFloat() + s
                    measuresEntry.add(RadarEntry(INDICATOR_RADIUS))
                }
            }

            when {
                (v == MIN_LIMIT) -> {
                    normalEntry.add(RadarEntry(v-OFFSET))
                    highMEntry.add(RadarEntry(v-OFFSET))
                    highHEntry.add(RadarEntry(v-OFFSET))

                    coverEntry.add(RadarEntry(v))
                    coverEntry2.add(RadarEntry(v))

                    lowMEntry.add(RadarEntry(v-OFFSET))
                    lowHEntry.add(RadarEntry(v-OFFSET))
                }
                (v < LOW_H_LIMIT) -> {
                    normalEntry.add(RadarEntry(v-OFFSET))
                    highMEntry.add(RadarEntry(v-OFFSET))
                    highHEntry.add(RadarEntry(v-OFFSET))

                    coverEntry.add(RadarEntry(v+ OFFSET))
                    coverEntry2.add(RadarEntry(v-COVER_OFFSET))

                    //lowMEntry.add(RadarEntry(LOW_M_LIMIT - v/100))
                    //lowHEntry.add(RadarEntry(LOW_H_LIMIT - v/100))
                    lowMEntry.add(RadarEntry(v-OFFSET))
                    lowHEntry.add(RadarEntry(v))
                }
                (v < LOW_M_LIMIT) -> {
                    normalEntry.add(RadarEntry(v-OFFSET))
                    highMEntry.add(RadarEntry(v-OFFSET))
                    highHEntry.add(RadarEntry(v-OFFSET))

                    coverEntry.add(RadarEntry(v+ OFFSET))
                    coverEntry2.add(RadarEntry(v-COVER_OFFSET))

                    //lowMEntry.add(RadarEntry(LOW_M_LIMIT))
                    //lowHEntry.add(RadarEntry(LOW_H_LIMIT))
                    lowMEntry.add(RadarEntry(v))
                    lowHEntry.add(RadarEntry(LOW_H_LIMIT))
                }
                (v < NORMAL_LIMIT) -> {
                    normalEntry.add(RadarEntry(v))
                    highMEntry.add(RadarEntry(v-OFFSET))
                    highHEntry.add(RadarEntry(v-OFFSET))

                    coverEntry.add(RadarEntry(LOW_M_LIMIT))
                    coverEntry2.add(RadarEntry(v-COVER_OFFSET))

                    lowMEntry.add(RadarEntry(LOW_M_LIMIT))
                    lowHEntry.add(RadarEntry(LOW_H_LIMIT))
                }
                (v < HIGH_M_LIMIT) -> {
                    normalEntry.add(RadarEntry(NORMAL_LIMIT))
                    highMEntry.add(RadarEntry(v))
                    highHEntry.add(RadarEntry(v-OFFSET))

                    coverEntry.add(RadarEntry(LOW_M_LIMIT))
                    coverEntry2.add(RadarEntry(v-COVER_OFFSET))

                    lowMEntry.add(RadarEntry(LOW_M_LIMIT))
                    lowHEntry.add(RadarEntry(LOW_H_LIMIT))
                }
                else -> {
                    normalEntry.add(RadarEntry(NORMAL_LIMIT))
                    highMEntry.add(RadarEntry(HIGH_M_LIMIT))
                    highHEntry.add(RadarEntry(v))

                    coverEntry.add(RadarEntry(LOW_M_LIMIT))
                    coverEntry2.add(RadarEntry(v-COVER_OFFSET))

                    lowMEntry.add(RadarEntry(LOW_M_LIMIT))
                    lowHEntry.add(RadarEntry(LOW_H_LIMIT))
                }
            }
        }

        val measuresSet = RadarDataSet(measuresEntry, "Measure")
        formatSet(measuresSet,
            Color.argb(60, 255, 255, 255),
            Color.BLACK,
            0
        )
        measuresSet.lineWidth = 2f

        val highHSet = RadarDataSet(highHEntry, "High")
        formatSet(highHSet,
            Color.argb(80, Color.red(HIGH_H_COLOR), Color.green(HIGH_H_COLOR), Color.blue(HIGH_H_COLOR)),
            HIGH_H_COLOR,
            180
        )

        val borderSet = RadarDataSet(highHEntry, "Border")
        formatSet(borderSet,
            Color.argb(100,30, 30, 30),
            Color.rgb(255, 255, 255),
            0
        )
        borderSet.lineWidth = 5f

        val highMSet = RadarDataSet(highMEntry, "Mid")
        formatSet(highMSet,
            Color.argb(80, Color.red(HIGH_M_COLOR), Color.green(HIGH_M_COLOR), Color.blue(HIGH_M_COLOR)),
            HIGH_M_COLOR,
            200
        )

        val normalSet = RadarDataSet(normalEntry, "Inner")
        formatSet(normalSet,
            Color.argb(80, Color.red(NORMAL_COLOR), Color.green(NORMAL_COLOR), Color.blue(NORMAL_COLOR)),
            NORMAL_COLOR,
            250
        )

        val lowMSet = RadarDataSet(lowMEntry, "Low Medium")
        formatSet(lowMSet,
            Color.argb(80, Color.red(LOW_M_COLOR), Color.green(LOW_M_COLOR), Color.blue(LOW_M_COLOR)),
            LOW_M_COLOR,
            200
        )

        val lowHSet = RadarDataSet(lowHEntry, "Low High")
        formatSet(lowHSet,
            Color.argb(80,180, 0, 180),
            LOW_H_COLOR,
            180
        )

        val coverSet = RadarDataSet(coverEntry2, "")
        formatSet(coverSet,
            COVER_COLOR,
            COVER_COLOR,
            255
        )

        val entry:  ArrayList<RadarEntry> = ArrayList(MutableList(DAY_UNITS) { RadarEntry(MIN_LIMIT) })

        val selectionSet = RadarDataSet(entry, "Selected")
        formatSet(selectionSet,
            Color.argb(250, 180, 64, 0),
            Color.BLACK,
            0
        )
        selectionSet.lineWidth = INDICATOR_LINE_WIDTH

        val sets: ArrayList<IRadarDataSet> = ArrayList()

        sets.add(highHSet)
        sets.add(highMSet)
        sets.add(normalSet)
        sets.add(lowMSet)
        sets.add(lowHSet)
        sets.add(coverSet)

        sets.add(getConstantRadarDataSet(LOW_H_LIMIT))
        sets.add(getConstantRadarDataSet(LOW_M_LIMIT))
        sets.add(getConstantRadarDataSet(NORMAL_LIMIT))
        sets.add(getConstantRadarDataSet(HIGH_M_LIMIT))

        sets.add(measuresSet)
        sets.add(borderSet)
        sets.add(selectionSet)


        rd = RadarData(sets)
        rd.setValueTextSize(8f)
        rd.setDrawValues(false)
        rd.setValueTextColor(Color.WHITE)

        return rd
    }

    private var lastValue = Data(Calendar.getInstance(), 0f)
    fun getLast(): Data {
        return lastValue
    }

    private fun formatSet (s: RadarDataSet, lineColor: Int, fillColor: Int, alpha: Int) {
        s.color = lineColor
        s.fillColor = fillColor
        s.fillAlpha = alpha
        s.setDrawFilled(true)
        s.lineWidth = 0f
        s.isDrawHighlightCircleEnabled = false
        s.setDrawHighlightIndicators(false)
        s.setDrawValues(false)
    }

    fun save() {
        try {
            File(context.filesDir, "test_save.csv").printWriter().use { out ->
                for (d in database) {
                    out.println("${d.time.timeInMillis},${d.value}\n")
                }
            }
            Log.d(TAG, "saved!")
        } catch (e: Exception) {
            Log.d(TAG, e.toString())
        }
    }

    fun add(value: Float) {
        database.add(Data(Calendar.getInstance(), value))
    }

    fun add(time: Calendar, value: Float) {
        database.add(Data(time, value))
    }

    fun load() {
        try {
            File(context.filesDir, "test_save.csv").bufferedReader().useLines { lines ->
                data.clear()
                database.clear()
                for (line in lines) {
                    val d = parseLine(line)
                    if (d != null) {
                        database.add(d)
                    }
                }
            }
            Log.d(TAG, "loaded!")
            Log.d(TAG, "min: " + minDate.toString())
            Log.d(TAG, "max: " + maxDate.toString())
            Log.d(TAG, "day: " + days.toString())
            dateIndex = days.size - 1
        } catch (e: Exception) {
            Log.d(TAG, e.toString())
        }

        /*
        Log.d(TAG, LOW_H_LIMIT.toString())
        Log.d(TAG, LOW_M_LIMIT.toString())
        Log.d(TAG, NORMAL_LIMIT.toString())
        Log.d(TAG, HIGH_M_LIMIT.toString())
        */
    }

    private fun parseLine(l: String): Data? {
        val args = l.split(",")
        return when (args.size) {
            2 -> {
                val t = Calendar.getInstance()
                t.timeInMillis = args[0].toLong()
                val v = args[1].toFloat()


                if (t > maxDate) {
                    maxDate = t
                    lastValue = Data(t, v)
                }
                if (t < minDate) minDate = t

                val date = t.clone() as Calendar
                date[Calendar.HOUR_OF_DAY] = 0
                date[Calendar.MINUTE] = 0
                date[Calendar.SECOND] = 0
                date[Calendar.MILLISECOND] = 0

                when (days.indexOfFirst { it == date }) {
                    -1 -> days.add(date)
                }

                Data(t, v)
            }
            else -> {
                null
            }
        }
    }

    private fun adjust(v: Float): Float{
        val x = (v - 5.0f) / 20
        val cubic = x.absoluteValue.pow(0.333f) * 3
        return when {
            x > 0 -> cubic + 3
            else -> 3 - cubic
        }
    }



    private val LOW_H_LIMIT = adjust(3.0f) + CHART_OFFSET
    private val LOW_M_LIMIT = adjust(4.4f) + CHART_OFFSET
    private val NORMAL_LIMIT = adjust(6.0f) + CHART_OFFSET
    private val HIGH_M_LIMIT = adjust(10.0f) + CHART_OFFSET
    private val HIGH_LIMIT = 20.0f + CHART_OFFSET

    companion object {
        private const val TAG = "CGx Glucose Data"

        private const val DAY_UNITS = 48
        private const val INDICATOR_RADIUS = 4.0f
        private const val INDICATOR_LINE_WIDTH = 5f

        private const val CHART_OFFSET = 4.5f

        const val MIN = 1
        const val MAX = 2

        private val MAX_GLUCOSE = 15.0f
        private val MIN_GLUCOSE = 2.0f

        private const val OFFSET = 0.02f

        private const val MIN_LIMIT = INDICATOR_RADIUS



        /*
        private val LOW_H_COLOR = Color.rgb(180, 0, 180)
        private val LOW_M_COLOR = Color.rgb(0, 0, 180)
        private val NORMAL_COLOR = Color.rgb(0, 180, 0)
        private val HIGH_M_COLOR = Color.rgb(255, 180, 0)
        private val HIGH_H_COLOR = Color.rgb(255, 0, 0)
        */

        private const val COVER_OFFSET = 1f

        //background-image: linear-gradient(to right, #0a03b1, #0057dc, #007ed2, #009ead, #00b88e, #39bb6e, #63bb46, #8cb800, #ada000, #ca8200, #e25b00, #f21212);
        val LOW_H_COLOR = Color.parseColor("#0a03b1")
        val LOW_M_COLOR = Color.parseColor("#007ed2")
        val NORMAL_COLOR = Color.parseColor("#63bb46")
        val HIGH_M_COLOR = Color.parseColor("#ca8200")
        val HIGH_H_COLOR = Color.parseColor("#f21212")

        val LOW_H_LEVEL = (3.0f)
        val LOW_M_LEVEL = (4.4f)
        val NORMAL_LEVEL = (6.0f)
        val HIGH_M_LEVEL = (10.0f)
        val HIGH_H_LEVEL = 20.0f

        private val COVER_COLOR = Color.parseColor("#202020")

        fun getLimit(value: Float): Float {
            return when {
                value < LOW_H_LEVEL -> {
                    LOW_H_LEVEL
                }
                value < LOW_M_LEVEL -> {
                    LOW_M_LEVEL
                }
                value < NORMAL_LEVEL -> {
                    NORMAL_LEVEL
                }
                value < HIGH_M_LEVEL -> {
                    HIGH_M_LEVEL
                }
                else -> {
                    HIGH_H_LEVEL
                }
            }
        }

        fun getColor(value: Float): Int {
            return when (getLimit(value)) {
                LOW_H_LEVEL -> LOW_H_COLOR
                LOW_M_LEVEL -> LOW_M_COLOR
                NORMAL_LEVEL -> NORMAL_COLOR
                HIGH_M_LEVEL -> HIGH_M_COLOR
                HIGH_H_LEVEL -> HIGH_H_COLOR
                else -> 0
            }
        }
    }
}
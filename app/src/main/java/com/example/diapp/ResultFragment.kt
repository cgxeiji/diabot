package com.example.diapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import kotlinx.android.synthetic.main.fragment_result.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "glucoseValue"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ResultFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ResultFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var glucoseValue: Float = 0f
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            glucoseValue = it.getFloat(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_result, container, false)

        view.circle.value = 11.8f
        view.circle.setColor(GlucoseData.getColor(glucoseValue))
        view.circle.translationY = 500f

        val transitionT: Long = 800

        view.circle.animate().setDuration(transitionT).setInterpolator(AccelerateInterpolator())
            .translationY(0f).start()

        view.tvValue.alpha = 0f
        view.tvValue.text = String.format("%.1f\nmmol/L", glucoseValue)
        view.tvValue.animate().setDuration(500).setInterpolator(LinearInterpolator())
            .alpha(1f).setStartDelay(transitionT).start()

        view.tvMsg.alpha = 0f
        view.tvMsg.animate().setDuration(500).setInterpolator(LinearInterpolator())
            .alpha(1f).setStartDelay(transitionT).start()

        view.imgIcon.alpha = 0f
        view.imgIcon.setImageResource(R.drawable.warning)

        val anim = AlphaAnimation(1f, 0f).apply {
            duration = 500
            interpolator = LinearInterpolator()
            repeatCount = Animation.INFINITE
            repeatMode = Animation.REVERSE
        }

        when (GlucoseData.getLimit(glucoseValue)) {
            GlucoseData.LOW_H_LEVEL -> {
                view.imgIcon.setImageResource(R.drawable.warning)
                view.imgIcon.startAnimation(anim)
                view.tvMsg.text = "Please,\ndo something now!"
                view.tvValue.text = "Too low!"
            }
            GlucoseData.HIGH_H_LEVEL -> {
                view.imgIcon.setImageResource(R.drawable.warning)
                view.imgIcon.startAnimation(anim)
                view.tvMsg.text = "Please,\ndo something now!"
                view.tvValue.text = "Too high!"
            }
            GlucoseData.LOW_M_LEVEL -> {
                view.imgIcon.setImageResource(R.drawable.surprise)
                view.tvMsg.text = "Outside the\nrecommended range"
                view.tvValue.text = "Low sugar"
            }
            GlucoseData.HIGH_M_LEVEL -> {
                view.imgIcon.setImageResource(R.drawable.surprise)
                view.tvMsg.text = "Outside the\nrecommended range"
                view.tvValue.text = "High sugar"
            }
            GlucoseData.NORMAL_LEVEL -> {
                view.imgIcon.setImageResource(R.drawable.cool)
                view.tvMsg.text = "\nEverything seems good!"
                view.tvValue.text = "In range"
            }
        }

        view.imgIcon.animate().setDuration(500).setInterpolator(LinearInterpolator())
            .alpha(1f).setStartDelay(transitionT).start()

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ResultFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(glucose: Float) =
            ResultFragment().apply {
                arguments = Bundle().apply {
                    putFloat(ARG_PARAM1, glucose)
                }
            }
    }
}

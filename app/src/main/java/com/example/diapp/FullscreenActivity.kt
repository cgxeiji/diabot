package com.example.diapp

import android.R.attr
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import co.lujun.lmbluetoothsdk.BluetoothController
import co.lujun.lmbluetoothsdk.base.BluetoothListener
import co.lujun.lmbluetoothsdk.base.State
import kotlinx.android.synthetic.main.activity_fullscreen.*
import java.util.*


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class FullscreenActivity : AppCompatActivity(),
    TestFragment.OnFragmentInteractionListener {

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private val mHideHandler = Handler()
    private var mVisible: Boolean = false
    private val mHideRunnable = Runnable { hide() }

    private val bluetooth = Bluetooth()

    private val history = HistoryFragment()
    private val testFragment = TestFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_fullscreen)

        mVisible = true

        hide()

        bluetooth.init(this)
        bluetooth.setOnReadAngleListener(object: Bluetooth.OnReadAngleListener {
            override fun onReadAngle(angle: Float) {
                //Log.d(TAG, "angle: " + angle.toString())
                runOnUiThread { knob.rotate(angle * 20) }
            }
        })
        bluetooth.connect()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        //(frag as TestFragment).selected("No")

        changeFragment("Home")

        knob.setOnKnobSelectedListener(object: Knob.OnKnobSelectedListener {
            var previous = 100

            override fun onKnobSelected(selection: String, angle: Int, direction: Int) {
                debug.text = selection

                when (frag) {
                    is TestFragment -> {
                        when (direction) {
                            Knob.LEFT -> changeFragment("Input")
                            Knob.RIGHT -> changeFragment("History")
                        }
                    }
                    is HistoryFragment -> {
                        (frag as HistoryFragment).selected(angle, direction)
                    }
                    is InputFragment -> {
                        (frag as InputFragment).knob(angle)
                    }
                    is ResultFragment -> {
                        changeFragment("History")
                    }
                }

                previous = angle
            }
        })
        //(frag as TestFragment).selected("No")

        frameScreen.setOnTouchListener { _, _ ->
            hide()
            true
        }

        window.decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            // Note that system bars will only be "visible" if none of the
            // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
            Handler().postDelayed({
                hide()
            }, 200)
            Log.d(TAG, "visibility: $visibility")
        }
    }

    fun changeFragment(name: String) {
        when (name) {
            "History" -> {
                supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .replace(R.id.frag, HistoryFragment(), "History Fragment")
                    .commit()
                knob.setMode(Knob.PREV_NEXT)
            }
            "Input" -> {
                supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .replace(R.id.frag, InputFragment(), "Input Fragment")
                    .commit()
                knob.setMode(Knob.PLUS_MINUS)
            }
            "Result" -> {
                supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.frag, ResultFragment.newInstance(value), "Result Fragment")
                    .commit()
                knob.setMode(Knob.CONTINUE)
            }
            "Home" -> {
                supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .replace(R.id.frag, TestFragment(), "Home Fragment")
                    .commit()
                knob.setMode(Knob.LOG_HIST)
            }
        }
    }

    var value: Float = 0f
    fun showResult(r: Float) {
        value = r
        changeFragment("Result")
    }

    override fun onDestroy() {
        super.onDestroy()

        bluetooth.release()
    }

    private fun hide() {
        // Hide UI first
        supportActionBar?.hide()
        mVisible = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        //mHideHandler.removeCallbacks(mShowPart2Runnable)
        //mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY.toLong())

        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                // Set the content to appear under the system bars so that the
                // content doesn't resize when the system bars hide and show.
                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                // Hide the nav bar and status bar
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN)

    }

    override fun onResume() {
        super.onResume()

        hide()
    }

    /*
    private fun show() {
        // Show the system bar
        fullscreen_content.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                        View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        mVisible = true

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable)
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

     */

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private val AUTO_HIDE_DELAY_MILLIS = 300

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private val UI_ANIMATION_DELAY = 300

        private val UUID_DEVICE = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb")

        private val TAG = "CGx FullscreenActivity"

        private const val BT_MAC = "98:D3:31:B3:86:48"
    }
}

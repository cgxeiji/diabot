package com.example.diapp

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import co.lujun.lmbluetoothsdk.BluetoothController
import co.lujun.lmbluetoothsdk.base.BluetoothListener
import co.lujun.lmbluetoothsdk.base.State
import java.lang.Exception
import java.util.*


class Bluetooth {
    lateinit var controller: BluetoothController
    private var tryConnection = MAX_TRY
    private val buffer = StringBuilder()
    private val handler = Handler(Looper.getMainLooper())

    fun init(context: Context) {
        fun postToastMessage(message: String?) {
            handler.post { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
        }

        controller = BluetoothController.getInstance().build(context)
        controller.appUuid = UUID_DEVICE
        controller.setBluetoothListener(object: BluetoothListener {
            override fun onActionStateChanged(preState: Int, state: Int) {
            }

            override fun onActionDiscoveryStateChanged(discoveryState: String) {
            }

            override fun onActionScanModeChanged(preScanMode: Int, scanMode: Int) {
            }

            override fun onBluetoothServiceStateChanged(state: Int) {
                Log.d(TAG, "service: $state")

                when (state) {
                    State.STATE_CONNECTED -> {
                        Log.d(TAG, "connected")
                        postToastMessage("Connected!")
                        tryConnection = MAX_TRY
                    }
                    State.STATE_LISTEN -> {
                        if (tryConnection > 0) {
                            Log.d(TAG, "trying to reconnect")
                            tryConnection -= 1
                            handler.postDelayed({
                                connect()
                            }, 2000)
                        }
                    }
                }
            }

            override fun onActionDeviceFound(device: BluetoothDevice, rssi: Short) {
                Log.d(TAG, "Found " + device.name + "@" + device.address)
            }

            override fun onReadData(device: BluetoothDevice?, data: ByteArray?) {
                val deviceName = device?.name

                buffer.append(String(data!!))
                val lines = buffer.lines()

                if (lines.size > 1) {
                    val angle = lines[0].toFloatOrNull()
                    //Log.d(TAG, deviceName + ": " + angle + "\n")

                    if (angle != null) mOnReadAngleListener?.onReadAngle(angle)
                    buffer.delete(0, lines[0].length + 2)
                }

            }
        })


    }

    fun connect() {
        try {
            controller.connect(BT_MAC)
        } catch (e: Exception) {
            Log.d(TAG, e.message)
        }
    }

    fun release() {
        controller.release()
    }

    interface OnReadAngleListener {
        fun onReadAngle(angle: Float)
    }

    private var mOnReadAngleListener: OnReadAngleListener? = null

    fun setOnReadAngleListener(l: OnReadAngleListener?) {
        this.mOnReadAngleListener = l
    }

    companion object {
        private const val TAG = "CGx Bluetooth"
        private val UUID_DEVICE = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb")
        private const val BT_MAC = "98:D3:31:B3:86:48"

        private const val MAX_TRY = 5
    }


}